contract Unlock {
    Privacy public privacy = Privacy(0x71705c051679b6d5efc9d4d1988bcdae93b5208a); 

    function unlock0() public {
        bytes32 one = 0x2c76d85a0d02a0006655264165f08be073a30abfa58ed46a0feedb57a847fc00;
        bytes16 two = 0x2c76d85a0d02a0006655264165f08be0;
        privacy.unlock(two);
    }
    
    function unlock1() public {
        bytes32 one = 0x2c76d85a0d02a0006655264165f08be073a30abfa58ed46a0feedb57a847fc00;
        bytes16 three = 0x73a30abfa58ed46a0feedb57a847fc00;
        privacy.unlock(three);
    }
    
}